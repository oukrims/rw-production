<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");

 
	$cat_qry="SELECT * FROM tbl_ringtone_category ORDER BY category_name";
	$cat_result=mysqli_query($mysqli,$cat_qry); 
	
	if(isset($_POST['submit']))
	{
 			
				
        if ($_POST['ringtone_type']=='server_url')
        {
              $ringtone_url=$_POST['ringtone_url'];

              $file_name= str_replace(" ","-",$_FILES['ringtone_thumbnail']['name']);

              $ringtone_thumbnail=rand(0,99999)."_".$file_name;
       
              //Main Image
              $tpath1='images/'.$ringtone_thumbnail;        
              $pic1=compress_image($_FILES["ringtone_thumbnail"]["tmp_name"], $tpath1, 80);
         
              //Thumb Image 
              $thumbpath='images/thumbs/'.$ringtone_thumbnail;   
              $thumb_pic1=create_thumb_image($tpath1,$thumbpath,'200','200');   

              $video_id='';

        } 

        if ($_POST['ringtone_type']=='local')
        {

              $file_path = 'http://'.$_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']).'/uploads/';
              
              $ringtone_url=$file_path.$_POST['mp3_file_name'];

              $file_name= str_replace(" ","-",$_FILES['ringtone_thumbnail']['name']);

              $ringtone_thumbnail=rand(0,99999)."_".$file_name;
       
              //Main Image
              $tpath1='images/'.$ringtone_thumbnail;        
              $pic1=compress_image($_FILES["ringtone_thumbnail"]["tmp_name"], $tpath1, 80);
         
              //Thumb Image 
              $thumbpath='images/thumbs/'.$ringtone_thumbnail;   
              $thumb_pic1=create_thumb_image($tpath1,$thumbpath,'200','200');   

              $video_id='';
        } 


          
        $data = array( 
          'user_id'  =>  $_SESSION['id'],
			    'ringtone_type'  =>  $_POST['ringtone_type'],
			    'ringtone_title'  =>  addslashes($_POST['ringtone_title']),
          'ringtone_url'  =>  $ringtone_url,
          'ringtone_thumbnail'  =>  $ringtone_thumbnail,
          'ringtone_duration'  =>  $_POST['ringtone_duration'],
          'status'  =>  1
			    );		

		 		$qry = Insert('tbl_ringtone',$data);	

 	    
		$_SESSION['msg']="10";
 
		header( "Location:add_ringtone.php");
		exit;	

		 
	}
	
	  
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>
            $(function () {
                $('#btn').click(function () {
                    $('.myprogress').css('width', '0');
                    $('.msg').text('');
                    var mp3_local = $('#mp3_local').val();
                    if (mp3_local == '') {
                        alert('Please enter file name and select file');
                        return;
                    }
                    var formData = new FormData();
                    formData.append('mp3_local', $('#mp3_local')[0].files[0]);
                    $('#btn').attr('disabled', 'disabled');
                     $('.msg').text('Uploading in progress...');
                    $.ajax({
                        url: 'uploadscript_mp3.php',
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        // this part is progress bar
                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    $('.myprogress').text(percentComplete + '%');
                                    $('.myprogress').css('width', percentComplete + '%');
                                }
                            }, false);
                            return xhr;
                        },
                        success: function (data) {
                         
                            $('#mp3_file_name').val(data);
                            $('.msg').text("File uploaded successfully!!");
                            $('#btn').removeAttr('disabled');
                        }
                    });
                });
            });
        </script>
<script type="text/javascript">
$(document).ready(function(e) {
           $("#ringtone_type").change(function(){
          
           var type=$("#ringtone_type").val();
              
           if(type=="server_url")
              {
                 
                 $("#video_url_display").show();
                 $("#thumbnail").show();
                 $("#video_local_display").hide();
              }
              else
              {   
                     
                $("#video_url_display").hide();               
                $("#video_local_display").show();
                $("#thumbnail").show();

              }    
              
         });
        });
</script>
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="page_title_block">
            <div class="col-md-5 col-xs-12">
              <div class="page_title">Add Ringtone</div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row mrg-top">
            <div class="col-md-12">
               
              <div class="col-md-12 col-sm-12">
                <?php if(isset($_SESSION['msg'])){?> 
               	 <div class="alert alert-success alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                	<?php echo $client_lang[$_SESSION['msg']] ; ?></a> </div>
                <?php unset($_SESSION['msg']);}?>	
              </div>
            </div>
          </div>
          <div class="card-body mrg_bottom"> 
            <form action="" name="add_form" method="post" class="form form-horizontal" enctype="multipart/form-data">
 
              <div class="section">
                <div class="section-body">
                   <div class="form-group">
                    
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Ringtone Title :-</label>
                    <div class="col-md-6">
                      <input type="text" name="ringtone_title" id="ringtone_title" value="" class="form-control" required>
                    </div>
                  </div>
                   <div class="form-group">
                    <label class="col-md-3 control-label">Ringtone duration :-</label>
                    <div class="col-md-6">
                      <input type="text" name="ringtone_duration" id="ringtone_duration" value="" class="form-control" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Ringtone Type :-</label>
                    <div class="col-md-6">                       
                      <select name="ringtone_type" id="ringtone_type" style="width:280px; height:25px;" class="select2" required>
                            <option value="">--Select Type--</option>                            
                            <option value="server_url">From Server</option>
                            <option value="local">From Local</option>
                      </select>
                    </div>
                  </div>
                  <div id="video_url_display" class="form-group">
                    <label class="col-md-3 control-label">Ringtone URL :-</label>
                    <div class="col-md-6">
                      <input type="text" name="ringtone_url" id="ringtone_url" value="" class="form-control">
                    </div>
                  </div>
                  <div id="video_local_display" class="form-group" style="display:none;">
                    <label class="col-md-3 control-label">Ringtone Upload :-</label>
                    <div class="col-md-6">
                    
                    <input type="hidden" name="mp3_file_name" id="mp3_file_name" value="" class="form-control">
                      <input type="file" name="mp3_local" id="mp3_local" value="" class="form-control">

                      <div class="progress">
                            <div class="progress-bar progress-bar-success myprogress" role="progressbar" style="width:0%">0%</div>
                        </div>

                        <div class="msg"></div>
                        <input type="button" id="btn" class="btn-success" value="Upload" />
                    </div>
                  </div><br>
                  <div id="thumbnail" class="form-group" style="display:none;">
                    <label class="col-md-3 control-label">Thumbnail Image:-</label>
                    <div class="col-md-6">
                      <div class="fileupload_block">
                        <input type="file" name="ringtone_thumbnail" value="" id="fileupload">
                       <div class="fileupload_img"><img type="image" src="assets/images/add-image.png" alt="category image" /></div>
                      </div>
                    </div>
                  </div>                  
                  
                  
                  <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
        
<?php include("includes/footer.php");?>       
