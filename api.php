<?php include("includes/connection.php");
 	  include("includes/function.php"); 	
	
	$file_path = 'http://'.$_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']).'/';
 	

 	 if(isset($_GET['home']))
	{
  
		$jsonObj1= array();	
	
	    $query1="SELECT * FROM tbl_wallpaper
		LEFT JOIN tbl_wallpaper_category ON tbl_wallpaper.cat_id= tbl_wallpaper_category.cid 
		WHERE tbl_wallpaper.status='1' ORDER BY tbl_wallpaper.id DESC LIMIT 15";

		$sql1 = mysqli_query($mysqli,$query1)or die(mysqli_error());

		while($data1 = mysqli_fetch_assoc($sql1))
		{
			$row1['id'] = $data1['id'];
			$row1['cat_id'] = $data1['cat_id'];
			$row1['user_id'] = $data1['user_id'];
			$row1['user_name'] = $data1['user_name'];	 
			 
			$row1['wallpaper_thumbnail_b'] = $file_path.'images/'.$data1['image'];
			$row1['wallpaper_thumbnail_s'] = $file_path.'images/thumbs/'.$data1['image'];		 
 
			$row1['tags'] = $data1['tags'];
			$row1['rate_avg'] = $data1['rate_avg'];
			$row1['total_rate'] = $data1['total_rate'];
			$row1['total_views'] = $data1['total_views'];
			$row1['total_download'] = $data1['total_download'];

			$row1['cid'] = $data1['cid'];
			$row1['category_name'] = $data1['category_name'];
			$row1['category_image'] = $file_path.'images/'.$data1['category_image'];
			$row1['category_image_thumb'] = $file_path.'images/thumbs/'.$data1['category_image'];
			 
			array_push($jsonObj1,$row1);
		
		}

		$row['wallpaper_latest'] = $jsonObj1;


		$jsonObj2= array();	
 
		$query2="SELECT * FROM tbl_video
		LEFT JOIN tbl_video_category ON tbl_video.cat_id= tbl_video_category.cid 
		WHERE tbl_video.status='1' ORDER BY tbl_video.id DESC LIMIT 15";

		$sql2 = mysqli_query($mysqli,$query2)or die(mysqli_error());

		while($data2 = mysqli_fetch_assoc($sql2))
		{
			$row2['id'] = $data2['id'];
			$row2['cat_id'] = $data2['cat_id'];
			$row2['user_id'] = $data2['user_id'];
			$row2['user_name'] = $data2['user_name'];
			$row2['video_type'] = $data2['video_type'];
			$row2['video_title'] = stripslashes($data2['video_title']);
			$row2['video_url'] = $data2['video_url'];
			$row2['video_id'] = $data2['video_id'];
			
			if($data2['video_type']=='server_url' or $data2['video_type']=='local')
			{
				$row2['video_thumbnail_b'] = $file_path.'images/'.$data2['video_thumbnail'];
				$row2['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data2['video_thumbnail'];
			}
			else
			{
				$row2['video_thumbnail_b'] = $file_path.'images/'.$data2['video_thumbnail'];
				$row2['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data2['video_thumbnail'];
			}
 
			$row2['video_duration'] = $data2['video_duration'];

			$row2['tags'] = $data2['tags'];
			$row2['total_views'] = $data2['total_views'];
			$row2['rate_avg'] = $data2['rate_avg'];
			$row2['total_rate'] = $data2['total_rate'];

			$row2['cid'] = $data2['cid'];
			$row2['category_name'] = $data2['category_name'];
			$row2['category_image'] = $file_path.'images/'.$data2['category_image'];
			$row2['category_image_thumb'] = $file_path.'images/thumbs/'.$data2['category_image'];
			 
			array_push($jsonObj2,$row2);
		
		} 
		$row['video_latest'] = $jsonObj2;


		$jsonObj3= array();	
	
	    $query3="SELECT * FROM tbl_ringtone
		
		WHERE tbl_ringtone.status='1' ORDER BY tbl_ringtone.id DESC LIMIT 15";

		$sql3 = mysqli_query($mysqli,$query3)or die(mysqli_error());

		while($data3 = mysqli_fetch_assoc($sql3))
		{
			$row3['id'] = $data3['id'];

			$row3['user_id'] = $data3['user_id'];
			$row3['user_name'] = $data3['user_name'];
			$row3['ringtone_type'] = $data3['ringtone_type'];
			$row3['ringtone_title'] = stripslashes($data3['ringtone_title']);
			$row3['ringtone_url'] = $data3['ringtone_url'];
			$row3['ringtone_duration'] = $data3['ringtone_duration'];
			 
			 
			$row3['ringtone_thumbnail_b'] = $file_path.'images/'.$data3['ringtone_thumbnail'];
			$row3['ringtone_thumbnail_s'] = $file_path.'images/thumbs/'.$data3['ringtone_thumbnail'];
		 
 
			$row3['rate_avg'] = $data3['rate_avg'];
			$row3['total_rate'] = $data3['total_rate'];
			$row3['total_views'] = $data3['total_views'];
			$row3['total_download'] = $data3['total_download'];

			
			array_push($jsonObj3,$row3);
		
		}
		
		$row['latest_ringtone'] = $jsonObj3;

		$set['FUNDRIVE_APP'] = $row;

		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();
		
	}
 	else if(isset($_GET['wall_cat_list']))
 	{
 		$jsonObj= array();
		
		$cat_order=API_CAT_ORDER_BY;


		$query="SELECT cid,category_name,category_image FROM tbl_wallpaper_category ORDER BY tbl_wallpaper_category.".$cat_order."";
		$sql = mysqli_query($mysqli,$query)or die(mysql_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			 
			
			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
			if($data['cid']!=0){
				array_push($jsonObj,$row);

			}
		
		}

		$set['FUNDRIVE_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();
 	}
	else if(isset($_GET['wall_cat_id']))
	{
		$post_order_by=API_CAT_POST_ORDER_BY;

		$cat_id=$_GET['wall_cat_id'];	

		$jsonObj= array();	
	
	    $query="SELECT * FROM tbl_wallpaper
		LEFT JOIN tbl_wallpaper_category ON tbl_wallpaper.cat_id= tbl_wallpaper_category.cid 
		where tbl_wallpaper.cat_id='".$cat_id."' AND tbl_wallpaper.status='1' ORDER BY tbl_wallpaper.id ".$post_order_by."";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];

			$row['user_id'] = $data['user_id'];
			$row['user_name'] = $data['user_name'];
			 
			 
			$row['wallpaper_thumbnail_b'] = $file_path.'images/'.$data['image'];
			$row['wallpaper_thumbnail_s'] = $file_path.'images/thumbs/'.$data['image'];
		 
 
			$row['tags'] = $data['tags'];
			$row['rate_avg'] = $data['rate_avg'];
			$row['total_rate'] = $data['total_rate'];
			$row['total_views'] = $data['total_views'];
			$row['total_download'] = $data['total_download'];

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
			 

			array_push($jsonObj,$row);
		
		}

		$set['FUNDRIVE_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

		
	}	 
	else if(isset($_GET['wall_latest']))
	{
		//$limit=$_GET['latest'];	 

		$limit=API_LATEST_LIMIT;

		$jsonObj= array();	 
	 
		$query="SELECT * FROM tbl_wallpaper
		LEFT JOIN tbl_wallpaper_category ON tbl_wallpaper.cat_id= tbl_wallpaper_category.cid 
		WHERE tbl_wallpaper.status='1' ORDER BY tbl_wallpaper.id DESC LIMIT $limit";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];
			
			$row['user_id'] = $data['user_id'];
			$row['user_name'] = $data['user_name']; 
			 
			$row['wallpaper_thumbnail_b'] = $file_path.'images/'.$data['image'];
			$row['wallpaper_thumbnail_s'] = $file_path.'images/thumbs/'.$data['image'];
		 
 
			$row['tags'] = $data['tags'];
			$row['rate_avg'] = $data['rate_avg'];
			$row['total_rate'] = $data['total_rate'];
			$row['total_views'] = $data['total_views'];
			$row['total_download'] = $data['total_download'];

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
			 
				array_push($jsonObj,$row);
			
		
		}

		$set['FUNDRIVE_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

	}
	else if(isset($_GET['wall_popular']))
	{
		//$limit=$_GET['latest'];	 

		$limit=API_LATEST_LIMIT;

		$jsonObj= array();	 
	 
		$query="SELECT * FROM tbl_wallpaper
		LEFT JOIN tbl_wallpaper_category ON tbl_wallpaper.cat_id= tbl_wallpaper_category.cid 
		WHERE tbl_wallpaper.status='1' ORDER BY tbl_wallpaper.total_views DESC LIMIT $limit";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];
			
			$row['user_id'] = $data['user_id'];
			$row['user_name'] = $data['user_name']; 
			 
			$row['wallpaper_thumbnail_b'] = $file_path.'images/'.$data['image'];
			$row['wallpaper_thumbnail_s'] = $file_path.'images/thumbs/'.$data['image'];
		 
 
			$row['tags'] = $data['tags'];
			$row['rate_avg'] = $data['rate_avg'];
			$row['total_rate'] = $data['total_rate'];
			$row['total_views'] = $data['total_views'];
			$row['total_download'] = $data['total_download'];

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
			 

			array_push($jsonObj,$row);
		
		}

		$set['FUNDRIVE_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

	}
	else if(isset($_GET['wall_id']))
	{
 
		$wall_id=$_GET['wall_id'];	

		$jsonObj= array();	
	
	    $query="SELECT * FROM tbl_wallpaper
		LEFT JOIN tbl_wallpaper_category ON tbl_wallpaper.cat_id= tbl_wallpaper_category.cid 
		where tbl_wallpaper.id='".$wall_id."'";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];
			
			$row['user_id'] = $data['user_id'];
			$row['user_name'] = $data['user_name']; 
			 
			$row['wallpaper_thumbnail_b'] = $file_path.'images/'.$data['image'];
			$row['wallpaper_thumbnail_s'] = $file_path.'images/thumbs/'.$data['image'];
		 
 
			$row['tags'] = $data['tags'];
			$row['rate_avg'] = $data['rate_avg'];
			$row['total_rate'] = $data['total_rate'];
			$row['total_views'] = $data['total_views'];
			$row['total_download'] = $data['total_download'];

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
			 

			array_push($jsonObj,$row);
		
		}

		$set['FUNDRIVE_APP'] = $jsonObj;

		$view_qry=mysqli_query($mysqli,"UPDATE tbl_wallpaper SET total_views = total_views + 1 WHERE id = '".$_GET['wall_id']."'");
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

		
	}
	else if(isset($_GET['wall_download_id']))
 	{
 		$wall_id=$_GET['wall_download_id'];	

 		$view_qry=mysqli_query($mysqli,"UPDATE tbl_wallpaper SET total_download = total_download + 1 WHERE id = '".$wall_id."'");

		$jsonObj= array();	
	
	    $query="SELECT * FROM tbl_wallpaper
		LEFT JOIN tbl_wallpaper_category ON tbl_wallpaper.cat_id= tbl_wallpaper_category.cid 
		where tbl_wallpaper.id='".$wall_id."'";
		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			 
			$row['total_download'] = $data['total_download'];		 
			array_push($jsonObj,$row);
		
		}

		$set['FUNDRIVE_APP'] = $jsonObj;
		 
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();
	 }
	 else if(isset($_GET['gif_list']))
 	{
 		$jsonObj= array();
		
		$gif_order=API_GIF_POST_ORDER_BY;


		$query="SELECT * FROM tbl_wallpaper_gif ORDER BY tbl_wallpaper_gif.total_views DESC LIMIT 23 ";
		$sql = mysqli_query($mysqli,$query)or die(mysql_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			 

			$row['id'] = $data['id'];			 
			$row['gif_image'] = $file_path.'images/animation/'.$data['image'];
			$row['total_views'] = $data['total_views'];

			 

			array_push($jsonObj,$row);
		
		}

		$set['FUNDRIVE_APP'] = $jsonObj;
	 
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();
 	}
 	else if(isset($_GET['gif_id']))
	{
		  
				 
		$jsonObj= array();	

		$query="SELECT * FROM tbl_wallpaper_gif WHERE id='".$_GET['gif_id']."'";
		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			 
			$row['id'] = $data['id'];			 
			$row['gif_image'] = $file_path.'images/animation/'.$data['image'];
 			$row['total_views'] = $data['total_views'];
 

			array_push($jsonObj,$row);
		
		}

		$view_qry=mysqli_query($mysqli,"UPDATE tbl_wallpaper_gif SET total_views = total_views + 1 WHERE id = '".$_GET['gif_id']."'");
 

		$set['FUNDRIVE_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();	
 

	}
	else if(isset($_GET['video_cat_list']))
 	{
 		$jsonObj= array();
		
		$cat_order=API_CAT_ORDER_BY;


		$query="SELECT cid,category_name,category_image FROM tbl_video_category ORDER BY tbl_video_category.".$cat_order."";
		$sql = mysqli_query($mysqli,$query)or die(mysql_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			 

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
 
			array_push($jsonObj,$row);
		
		}

		$set['FUNDRIVE_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();
 	}
	else if(isset($_GET['video_cat_id']))
	{
		$post_order_by=API_CAT_POST_ORDER_BY;

		$cat_id=$_GET['video_cat_id'];	

		$jsonObj= array();	
	
	    $query="SELECT * FROM tbl_video
		LEFT JOIN tbl_video_category ON tbl_video.cat_id= tbl_video_category.cid 
		where tbl_video.cat_id='".$cat_id."' AND tbl_video.status='1' ORDER BY tbl_video.id ".$post_order_by."";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];
			$row['user_id'] = $data['user_id'];
			$row['user_name'] = $data['user_name'];
			$row['video_type'] = $data['video_type'];
			$row['video_title'] = stripslashes($data['video_title']);
			$row['video_url'] = $data['video_url'];
			$row['video_id'] = $data['video_id'];
			
			if($data['video_type']=='server_url' or $data['video_type']=='local')
			{
				$row['video_thumbnail_b'] = $file_path.'images/'.$data['video_thumbnail'];
				$row['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data['video_thumbnail'];
			}
			else
			{
				$row['video_thumbnail_b'] = $file_path.'images/'.$data['video_thumbnail'];
				$row['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data['video_thumbnail'];
			}
 
			$row['video_duration'] = $data['video_duration'];
 
			$row['tags'] = $data['tags'];
			$row['total_views'] = $data['total_views'];
			$row['rate_avg'] = $data['rate_avg'];
			$row['total_rate'] = $data['total_rate'];

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
			 

			array_push($jsonObj,$row);
		
		}

		$set['FUNDRIVE_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

		
	}	 
	else if(isset($_GET['video_latest']))
	{
		//$limit=$_GET['latest'];	 

		$limit=API_LATEST_LIMIT;

		$jsonObj= array();	
 
		$query="SELECT * FROM tbl_video
		LEFT JOIN tbl_video_category ON tbl_video.cat_id= tbl_video_category.cid 
		WHERE tbl_video.status='1' ORDER BY tbl_video.id DESC LIMIT $limit";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];
			$row['user_id'] = $data['user_id'];
			$row['user_name'] = $data['user_name'];
			$row['video_type'] = $data['video_type'];
			$row['video_title'] = stripslashes($data['video_title']);
			$row['video_url'] = $data['video_url'];
			$row['video_id'] = $data['video_id'];
			
			if($data['video_type']=='server_url' or $data['video_type']=='local')
			{
				$row['video_thumbnail_b'] = $file_path.'images/'.$data['video_thumbnail'];
				$row['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data['video_thumbnail'];
			}
			else
			{
				$row['video_thumbnail_b'] = $file_path.'images/'.$data['video_thumbnail'];
				$row['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data['video_thumbnail'];
			}
 
			$row['video_duration'] = $data['video_duration'];
 
			$row['tags'] = $data['tags'];
			$row['total_views'] = $data['total_views'];
			$row['rate_avg'] = $data['rate_avg'];
			$row['total_rate'] = $data['total_rate'];

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];			 
			 

			array_push($jsonObj,$row);
		
		}

		$set['FUNDRIVE_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

	}
	else if(isset($_GET['video_popular']))
	{
		//$limit=$_GET['latest'];	 

		$limit=API_LATEST_LIMIT;

		$jsonObj= array();	
 
		$query="SELECT * FROM tbl_video
		LEFT JOIN tbl_video_category ON tbl_video.cat_id= tbl_video_category.cid 
		WHERE tbl_video.status='1' ORDER BY tbl_video.total_views DESC LIMIT $limit";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];
			$row['user_id'] = $data['user_id'];
			$row['user_name'] = $data['user_name'];
			$row['video_type'] = $data['video_type'];
			$row['video_title'] = stripslashes($data['video_title']);
			$row['video_url'] = $data['video_url'];
			$row['video_id'] = $data['video_id'];
			
			if($data['video_type']=='server_url' or $data['video_type']=='local')
			{
				$row['video_thumbnail_b'] = $file_path.'images/'.$data['video_thumbnail'];
				$row['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data['video_thumbnail'];
			}
			else
			{
				$row['video_thumbnail_b'] = $file_path.'images/'.$data['video_thumbnail'];
				$row['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data['video_thumbnail'];
			}
 
			$row['video_duration'] = $data['video_duration'];
 
			$row['tags'] = $data['tags'];
			$row['total_views'] = $data['total_views'];
			$row['rate_avg'] = $data['rate_avg'];
			$row['total_rate'] = $data['total_rate'];

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];			 
			 

			array_push($jsonObj,$row);
		
		}

		$set['FUNDRIVE_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

	}
	else if(isset($_GET['video_id']))
	{ 
		$jsonObj= array();	
 
		$query="SELECT * FROM tbl_video
		LEFT JOIN tbl_video_category ON tbl_video.cat_id= tbl_video_category.cid 
		WHERE tbl_video.status='".$_GET['video_id']."'";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];
			$row['user_id'] = $data['user_id'];
			$row['user_name'] = $data['user_name'];
			$row['video_type'] = $data['video_type'];
			$row['video_title'] = stripslashes($data['video_title']);
			$row['video_url'] = $data['video_url'];
			$row['video_id'] = $data['video_id'];
			
			if($data['video_type']=='server_url' or $data['video_type']=='local')
			{
				$row['video_thumbnail_b'] = $file_path.'images/'.$data['video_thumbnail'];
				$row['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data['video_thumbnail'];
			}
			else
			{
				$row['video_thumbnail_b'] = $file_path.'images/'.$data['video_thumbnail'];
				$row['video_thumbnail_s'] = $file_path.'images/thumbs/'.$data['video_thumbnail'];
			}
 
			$row['video_duration'] = $data['video_duration'];
 
			$row['tags'] = $data['tags'];
			$row['total_views'] = $data['total_views'];
			$row['rate_avg'] = $data['rate_avg'];
			$row['total_rate'] = $data['total_rate'];

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];			 
			 

			array_push($jsonObj,$row);
		
		}

		$view_qry=mysqli_query($mysqli,"UPDATE tbl_video SET total_views = total_views + 1 WHERE id = '".$_GET['video_id']."'");

		$set['FUNDRIVE_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

	}
	else if(isset($_GET['ringtone_latest'])){
		 
		$limit=API_LATEST_LIMIT;
  
		$jsonObj= array();	
	
	    $query="SELECT * FROM tbl_ringtone
	
		WHERE tbl_ringtone.status='1' ORDER BY tbl_ringtone.id DESC LIMIT $limit";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];

			$row['user_id'] = $data['user_id'];
			$row['user_name'] = $data['user_name'];
			$row['ringtone_type'] = $data['ringtone_type'];
			$row['ringtone_title'] = stripslashes($data['ringtone_title']);
			$row['ringtone_url'] = $data['ringtone_url'];
			$row['ringtone_duration'] = $data['ringtone_duration'];
			 
			 
			$row['ringtone_thumbnail_b'] = $file_path.'images/'.$data['ringtone_thumbnail'];
			$row['ringtone_thumbnail_s'] = $file_path.'images/thumbs/'.$data['ringtone_thumbnail'];
		 
 
			$row['tags'] = $data['tags'];
			$row['rate_avg'] = $data['rate_avg'];
			$row['total_rate'] = $data['total_rate'];
			$row['total_views'] = $data['total_views'];
			$row['total_download'] = $data['total_download'];

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
			 

			array_push($jsonObj,$row);
		
		}

		$set['FUNDRIVE_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

	}
	else if(isset($_GET['ringtone_popular']))
	{
		//$limit=$_GET['latest'];	 

		$limit=API_LATEST_LIMIT;
 
		$jsonObj= array();	
	
	    $query="SELECT * FROM tbl_ringtone
		
		WHERE tbl_ringtone.status='1' ORDER BY tbl_ringtone.total_views DESC LIMIT $limit";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];

			$row['user_id'] = $data['user_id'];
			$row['user_name'] = $data['user_name'];
			$row['ringtone_type'] = $data['ringtone_type'];
			$row['ringtone_title'] = stripslashes($data['ringtone_title']);
			$row['ringtone_url'] = $data['ringtone_url'];
			$row['ringtone_duration'] = $data['ringtone_duration'];
			 
			 
			$row['ringtone_thumbnail_b'] = $file_path.'images/'.$data['ringtone_thumbnail'];
			$row['ringtone_thumbnail_s'] = $file_path.'images/thumbs/'.$data['ringtone_thumbnail'];
		 
 
			$row['tags'] = $data['tags'];
			$row['rate_avg'] = $data['rate_avg'];
			$row['total_rate'] = $data['total_rate'];
			$row['total_views'] = $data['total_views'];
			$row['total_download'] = $data['total_download'];

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
			 

			array_push($jsonObj,$row);
		
		}

		$set['FUNDRIVE_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

	}
	else if(isset($_GET['ringtone_id']))
	{
		 
		$ringtone_id=$_GET['ringtone_id'];	

		$jsonObj= array();	 
	    
		$query="SELECT * FROM tbl_ringtone
		WHERE tbl_ringtone.id='".$ringtone_id."'";

		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			$row['id'] = $data['id'];
			$row['cat_id'] = $data['cat_id'];

			$row['user_id'] = $data['user_id'];
			$row['user_name'] = $data['user_name'];
			$row['ringtone_type'] = $data['ringtone_type'];
			$row['ringtone_title'] = stripslashes($data['ringtone_title']);
			$row['ringtone_url'] = $data['ringtone_url'];
			$row['ringtone_duration'] = $data['ringtone_duration'];
			 
			 
			$row['ringtone_thumbnail_b'] = $file_path.'images/'.$data['ringtone_thumbnail'];
			$row['ringtone_thumbnail_s'] = $file_path.'images/thumbs/'.$data['ringtone_thumbnail'];
		 
 
			$row['tags'] = $data['tags'];
			$row['rate_avg'] = $data['rate_avg'];
			$row['total_rate'] = $data['total_rate'];
			$row['total_views'] = $data['total_views'];
			$row['total_download'] = $data['total_download'];

			$row['cid'] = $data['cid'];
			$row['category_name'] = $data['category_name'];
			$row['category_image'] = $file_path.'images/'.$data['category_image'];
			$row['category_image_thumb'] = $file_path.'images/thumbs/'.$data['category_image'];
			 

			array_push($jsonObj,$row);
		
		}

		$set['FUNDRIVE_APP'] = $jsonObj;

		$view_qry=mysqli_query($mysqli,"UPDATE tbl_ringtone SET total_views = total_views + 1 WHERE id = '".$ringtone_id."'");
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

		
	}
	else if(isset($_GET['ringtone_download_id']))
 	{
 		$ringtone_download_id=$_GET['ringtone_download_id'];	

 		$view_qry=mysqli_query($mysqli,"UPDATE tbl_ringtone SET total_download = total_download + 1 WHERE id = '".$ringtone_download_id."'");

		$jsonObj= array();	
	
	   $query="SELECT * FROM tbl_ringtone
		WHERE tbl_ringtone.id='".$ringtone_download_id."'";
		
		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			 
			$row['total_download'] = $data['total_download'];		 
			array_push($jsonObj,$row);
		
		}

		$set['FUNDRIVE_APP'] = $jsonObj;
		 
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();
 	}
 	else if(isset($_GET['feedback']))
 	{
 			$feed_name=$_GET['feed_name'];
 			$feed_email=$_GET['feed_email'];
            $feed_msg=$_GET['feed_msg'];


 			$data = array(            
               'name'  =>$feed_name,
               'email'  =>$feed_email,
               'message'  =>  $feed_msg
               );  
 			$qry = Insert('tbl_feedback',$data);

 			echo '{"FUNDRIVE_APP":[{"MSG":"Feedback succesfully submit..."}]}';
 			die();
 	}
 	else if(isset($_GET['android_token']))
	{

	  $qry="SELECT * FROM tbl_user_token WHERE token='".$_GET['android_token']."'";
      $result=mysqli_query($mysqli,$qry);
      $row=mysqli_fetch_assoc($result);
      
      if($row['token']==$_GET['android_token'])
      {
       		$set['FUNDRIVE_APP'][]=array('msg' => "token already added",'success'=>'0');
      }
      else
	  {
			 
		     $data = array(            
               'token'  =>  $_GET['android_token'],
               );  
      
     		$qry = Insert('tbl_user_token',$data);

            $set['FUNDRIVE_APP'][]=array('msg' => 'Success','Success'=>'1');
				
	  }

 
	 	header( 'Content-Type: application/json; charset=utf-8' );
    	echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();

	}	 		  	 
	else 
	{
		$jsonObj= array();	

		$query="SELECT * FROM tbl_settings WHERE id='1'";
		$sql = mysqli_query($mysqli,$query)or die(mysqli_error());

		while($data = mysqli_fetch_assoc($sql))
		{
			 
			$row['app_name'] = stripslashes($data['app_name']);
			$row['app_logo'] = $data['app_logo'];
			$row['app_version'] = $data['app_version'];
			$row['app_author'] = $data['app_author'];
			$row['app_contact'] = $data['app_contact'];
			$row['app_email'] = $data['app_email'];
			$row['app_website'] = $data['app_website'];
			$row['app_description'] = stripslashes($data['app_description']);
 			$row['app_developed_by'] = $data['app_developed_by'];

			$row['app_privacy_policy'] = stripslashes($data['app_privacy_policy']);
	

			array_push($jsonObj,$row);
		
		}

		$set['FUNDRIVE_APP'] = $jsonObj;
		
		header( 'Content-Type: application/json; charset=utf-8' );
	    echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
		die();	
	}		
	 
	 
?>