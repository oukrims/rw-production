<?php include("includes/header.php");

$file_path = 'http://'.$_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']).'/';
?>
<div class="row">
      <div class="col-sm-12 col-xs-12">
     	 	<div class="card">
		        <div class="card-header">
		          Example API urls
		        </div>
       			    <div class="card-body no-padding">
         		
         			 <pre><code class="html"><b>Home</b><br><?php echo $file_path."api.php?home"?><br><br><b>Latest Wallpaper</b><br><?php echo $file_path."api.php?wall_latest"?><br><br><b>Popular Wallpaper</b><br><?php echo $file_path."api.php?wall_popular"?><br><br><b>Wallpaper Category List</b><br><?php echo $file_path."api.php?wall_cat_list"?><br><br><b>Wallpaper list by Cat ID</b><br><?php echo $file_path."api.php?wall_cat_id=1"?><br><br><b>Single Wallpaper</b><br><?php echo $file_path."api.php?wall_id=3"?><br><br><b>Wallpaper Download</b><br><?php echo $file_path."api.php?wall_download_id=3"?><br><br><b>Latest Video</b><br><?php echo $file_path."api.php?video_latest"?><br><br><b>Popular Video</b><br><?php echo $file_path."api.php?video_popular"?><br><br><b>Video Category List</b><br><?php echo $file_path."api.php?video_cat_list"?><br><br><b>Video list by Cat ID</b><br><?php echo $file_path."api.php?video_cat_id=4"?><br><br><b>Single Video</b><br><?php echo $file_path."api.php?video_id=1"?><br><br><b>Latest Ringtone</b><br><?php echo $file_path."api.php?ringtone_latest"?><br><br><b>Popular Ringtone</b><br><?php echo $file_path."api.php?ringtone_popular"?><br><br><b>Ringtone Category List</b><br><?php echo $file_path."api.php?ringtone_cat_list"?><br><br><b>Ringtone list by Cat ID</b><br><?php echo $file_path."api.php?ringtone_cat_id=1"?><br><br><b>Single Ringtone</b><br><?php echo $file_path."api.php?ringtone_id=1"?><br><br><b>Ringtone Download</b><br><?php echo $file_path."api.php?ringtone_download_id=1"?><br><br><b>Rating (User post_type=wallpaper or video or ringtone)</b><br><?php echo $file_path."api_rating.php?post_id=1&post_type=wallpaper&rate=4&device_id=123"?><br><br><b>User Register</b><br><?php echo $file_path."user_register_api.php?name=john&email=john@gmail.com&password=123456&phone=1234567891"?><br><br><b>User FB Register</b><br><?php echo $file_path."user_register_fb_api.php?name=kuldip&email=kuldip@viaviweb.com&phone=1234567891&fb_id=12345678"?><br><br><b>User Gplus Register</b><br><?php echo $file_path."user_register_gplus_api.php?name=kuldip&email=kuldip@viaviweb.com&gplus_id=12345678"?><br><br><b>User Login</b><br><?php echo $file_path."user_login_api.php?email=john@gmail.com&password=123456"?><br><br><b>User Profile</b><br><?php echo $file_path."user_profile_api.php?id=2"?><br><br><b>User Profile Update</b><br><?php echo $file_path."user_profile_update_api.php?user_id=2&name=john&email=john@gmail.com&password=123456&phone=1234567891&city=rajkot&address=mayani chock"?><br><br><b>Forgot Password</b><br><?php echo $file_path."user_forgot_pass_api.php?email=john@gmail.com"?><br><br><b>User Token Reg.</b><br><?php echo $file_path."api.php?android_token=123"?><br><br><b>User Wallpaper Upload</b><br><?php echo $file_path."api_wallpaper.php"?>
<br><?php echo $file_path."api_wallpaper.php?cat_id=1&user_id=2&user_name=kuldip&wallpaper_image_name=test.png&tags=nice"?><br><br><b>User Ringtone Upload</b><br>Ringtone file upload<br><?php echo $file_path."api_ringtone_upload.php"?><br>Ringtone thumb upload<br><?php echo $file_path."api_ringtone_thumb.php"?><br>Ringtone upload<br><?php echo $file_path."api_ringtone.php?cat_id=1&user_id=2&user_name=kuldip&ringtone_type=local&ringtone_title=ringtone1&ringtone_url=#&ringtone_thumbnail=test.png&ringtone_duration=1:30&tags=nice"?><br><br><b>User Video Upload</b><br>Video file upload<br><?php echo $file_path."api_video_upload.php"?><br>Video thumb upload<br><?php echo $file_path."api_video_thumb.php"?><br>Video upload<br><?php echo $file_path."api_video.php?cat_id=1&user_id=2&user_name=kuldip&video_type=local&video_title=vidoe1&video_url=#&video_thumbnail=test.png&video_duration=1:30&video_description=test&tags=nice"?><br><br><b>Feedback</b><br><?php echo $file_path."api.php?feedback&feed_name=kuldip&feed_email=kuldip@viaviweb.com&feed_msg=test"?><br><br><b>App Details</b><br><?php echo $file_path."api.php"?></code></pre>
       		
       				</div>
          	</div>
        </div>
</div>
    <br/>
    <div class="clearfix"></div>
        
<?php include("includes/footer.php");?>       
