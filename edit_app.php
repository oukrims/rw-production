<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");

	require_once("thumbnail_images.class.php");

  

  $qry="SELECT * FROM apps where id='".$_GET['app_id']."'";
  $result=mysqli_query($mysqli,$qry);
  $row=mysqli_fetch_assoc($result);
	
	if(isset($_POST['submit']))
	{
        
        
        $file_name= str_replace(" ","-",$_FILES['app_icon']['name']);

        $app_thumb=rand(0,99999)."_".$file_name;

        //Main Image 
        $tpath1='images/'.$app_thumb;        
        $app_icon=compress_image($_FILES["app_icon"]["tmp_name"], $tpath1, 80);


        //Thumb Image 
        $thumbpath='images/thumbs/'.$app_thumb;   
        $thumb_pic1=create_thumb_image($tpath1,$thumbpath,'200','200');   


      
        $data = array( 
          'app_name'  =>  addslashes($_POST['app_name']),
          'app_desc'  =>  $_POST['app_desc'],
			    'app_icon'  =>  $_POST['app_icon'],
          'app_url'  =>  $_POST['app_url'],
          'app_icon'  =>  $app_icon
          );
  		 		 
    $qry=Update('apps', $data, "WHERE id = '".$_POST['app_id']."'");

         
		$_SESSION['msg']="11"; 
		header( "Location:edit_app.php?app_id=".$_POST['app_id']);
		exit;	

		 
	}
	
	  
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>
            $(function () {
                $('#btn').click(function () {
                    $('.myprogress').css('width', '0');
                    $('.msg').text('');
                    var video_local = $('#video_local').val();
                    if (video_local == '') {
                        alert('Please enter file name and select file');
                        return;
                    }
                    var formData = new FormData();
                    formData.append('video_local', $('#video_local')[0].files[0]);
                    $('#btn').attr('disabled', 'disabled');
                     $('.msg').text('Uploading in progress...');
                    $.ajax({
                        url: 'uploadscript.php',
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        // this part is progress bar
                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    $('.myprogress').text(percentComplete + '%');
                                    $('.myprogress').css('width', percentComplete + '%');
                                }
                            }, false);
                            return xhr;
                        },
                        success: function (data) {
                         
                            $('#video_file_name').val(data);
                            $('.msg').text("File uploaded successfully!!");
                            $('#btn').removeAttr('disabled');
                        }
                    });
                });
            });
        </script>
<script type="text/javascript">
$(document).ready(function(e) {
           $("#video_type").change(function(){
          
           var type=$("#video_type").val();
              
              if(type=="youtube" || type=="vimeo" || type=="dailymotion" || type=="server_url")
              { 
                //alert(type);
                $("#video_url_display").show();
                $("#video_local_display").hide();
                //$("#thumbnail").hide();
              } 
              else
              {                 
                $("#video_url_display").hide();               
                $("#video_local_display").show();
                $("#thumbnail").show();

              }    
              
         });
        });
</script>
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="page_title_block">
            <div class="col-md-5 col-xs-12">
              <div class="page_title">Edit App</div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row mrg-top">
            <div class="col-md-12">
               
              <div class="col-md-12 col-sm-12">
                <?php if(isset($_SESSION['msg'])){?> 
               	 <div class="alert alert-success alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                	<?php echo $client_lang[$_SESSION['msg']] ; ?></a> </div>
                <?php unset($_SESSION['msg']);}?>	
              </div>
            </div>
          </div>
          <div class="card-body mrg_bottom"> 
            <form action="" name="edit_form" method="post" class="form form-horizontal" enctype="multipart/form-data">
              <input  type="hidden" name="app_id" value="<?php echo $_GET['app_id'];?>" />

              <div class="section">
                <div class="section-body">
                   <div class="form-group">
                    <label class="col-md-3 control-label">App name :-</label>
                    <div class="col-md-6">
                    <input type="text" name="app_name" id="app_name" value="" class="form-control" required>

                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">App descreption :-</label>
                    <div class="col-md-6">
                    <textarea type="text" name="app_desc" id="video_title" value="" class="form-control" required></textarea>

                    </div>
                  </div>
                  
                  
                <br>
                  <div id="thumbnail" class="form-group">
                    <label class="col-md-3 control-label">App icon:-</label>
                    <div class="col-md-6">
                      <div class="fileupload_block">
                        <input type="file" name="app_icon" value="" id="fileupload">
                       <?php if(isset($_GET['app_id']) and $row['app_icon']!="") {?>
                       <input type="hidden" name="app_icon" id="app_icon" value="<?php echo $row['app_icon'];?>" class="form-control">
                            <div class="fileupload_img"><img type="image" src="images/<?php echo $row['app_icon'];?>" alt="app icon" /></div>
                          <?php } else {?>
                            <div class="fileupload_img"><img type="image" src="assets/images/add-image.png" alt="category image" /></div>
                          <?php }?>
                      </div>
                    </div>
                  </div>
                   
                 
                  <div class="form-group">
                    <label class="col-md-3 control-label">App store url :-</label>
                    <div class="col-md-6">
                    <input type="text" name="app_url" id="tags" value="" class="form-control">
                    </div>
                  </div> 
                  <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
        
<?php include("includes/footer.php");?>       
