<?php include("includes/header.php");




$qry_wallpaper="SELECT COUNT(*) as num FROM tbl_wallpaper";
$total_wallpaper= mysqli_fetch_array(mysqli_query($mysqli,$qry_wallpaper));
$total_wallpaper = $total_wallpaper['num'];

$qry_gif="SELECT COUNT(*) as num FROM tbl_wallpaper where cid = '0'";
$total_gif= mysqli_fetch_array(mysqli_query($mysqli,$qry_wallpaper));
$total_gif = $total_gif['num'];

$qry_ringtone="SELECT COUNT(*) as num FROM tbl_ringtone";
$total_ringtone= mysqli_fetch_array(mysqli_query($mysqli,$qry_ringtone));
$total_ringtone = $total_ringtone['num']; 

$qry_users="SELECT COUNT(*) as num FROM tbl_users";
$total_users= mysqli_fetch_array(mysqli_query($mysqli,$qry_users));
$total_users = $total_users['num'];


$qry_feedback="SELECT COUNT(*) as num FROM tbl_feedback";
$total_feedback= mysqli_fetch_array(mysqli_query($mysqli,$qry_feedback));
$total_feedback = $total_feedback['num'];


$qry_notification="SELECT COUNT(*) as num FROM tbl_normal_notification";
$total_notification= mysqli_fetch_array(mysqli_query($mysqli,$qry_notification));
$total_notification = $total_notification['num'];
?>       


        <div class="btn-floating" id="help-actions">
      <div class="btn-bg"></div>
      <button type="button" class="btn btn-default btn-toggle" data-toggle="toggle" data-target="#help-actions"> <i class="icon fa fa-plus"></i> <span class="help-text">Shortcut</span> </button>
      <div class="toggle-content">
       
      </div>
    </div>
    <div class="row">
       
        </div>
         <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12"> <a href="manage_wallpaper.php" class="card card-banner card-green-light">
        <div class="card-body"> <i class="icon fa fa-image fa-4x"></i>
          <div class="content">
            <div class="title">Wallpaper</div>
            <div class="value"><span class="sign"></span><?php echo $total_wallpaper;?></div>
          </div>
        </div>
        </a> 
        </div>
         <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12"> <a href="manage_ringtones.php" class="card card-banner card-orange-light">
        <div class="card-body"> <i class="icon fa fa-music fa-4x"></i>
          <div class="content">
            <div class="title">Ringtone</div>
            <div class="value"><span class="sign"></span><?php echo $total_ringtone;?></div>
          </div>
        </div>
        </a> 
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12"> <a href="manage_wallpaper_animation.php" class="card card-banner card-orange-light">
        <div class="card-body"> <i class="icon fa fa-music fa-4x"></i>
          <div class="content">
            <div class="title">GIFs</div>
            <div class="value"><span class="sign"></span><?php echo $total_gif;?></div>
          </div>
        </div>
        </a> 
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12"> <a href="manage_users.php" class="card card-banner card-blue-light">
        <div class="card-body"> <i class="icon fa fa-users fa-4x"></i>
          <div class="content">
            <div class="title">Users</div>
            <div class="value"><span class="sign"></span><?php echo $total_users;?></div>
          </div>
        </div>
        </a> 
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12"> <a href="manage_feedback.php" class="card card-banner card-red-light">
        <div class="card-body"> <i class="icon fa fa-comments fa-4x"></i>
          <div class="content">
            <div class="title">Feedback</div>
            <div class="value"><span class="sign"></span><?php echo $total_feedback;?></div>
          </div>
        </div>
        </a> 
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12"> <a href="manage_notification.php" class="card card-banner card-purple-light">
        <div class="card-body"> <i class="icon fa fa-send fa-4x"></i>
          <div class="content">
            <div class="title">Notification</div>
            <div class="value"><span class="sign"></span><?php echo $total_notification;?></div>
          </div>
        </div>
        </a> 
        </div>
     
    </div>

        
<?php include("includes/footer.php");?>       
