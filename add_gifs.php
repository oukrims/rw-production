<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");

 
	$cat_qry="SELECT * FROM tbl_wallpaper_category ORDER BY category_name";
	$cat_result=mysqli_query($mysqli,$cat_qry); 
	
	if(isset($_POST['submit']))
	{

		$count = count($_FILES['gifs']['name']);
		for($i=0;$i<$count;$i++)
		{  
      $albumimgnm="gif_".rand(0,99999)."_".$_FILES['gifs']['name'][$i];
			  
      $tpath1='images/'.$albumimgnm;	

       $pic1=$_FILES['gifs']['tmp_name'][$i];   
         
       //copy($pic1,$tpath1);
       move_uploaded_file($pic1, $tpath1) or die("Can't move file to $tpath1");

			 $thumbpath='images/thumbs/'.$albumimgnm;				
       move_uploaded_file($pic1,$thumbpath);   			
					   
			 $date=date('Y-m-j');								
				 
          
		    $data = array( 
					    'cat_id'  =>  0,
              'user_id'  =>  $_SESSION['id'],
              'user_name'  =>  $_POST['user_name'],
					    'image_date'  =>  $date,
					    'image'  =>  $albumimgnm,
              'tags'  =>  $_POST['tags'],
              'status'  => 1
					    );		

		 		$qry = Insert('tbl_wallpaper',$data);	

 	     }			

		$_SESSION['msg']="10";
 
		header( "Location:manage_wallpaper_animation.php");
		exit;	

		 
	}
	
	  
?>
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="page_title_block">
            <div class="col-md-5 col-xs-12">
              <div class="page_title">Add Gifs</div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row mrg-top">
            <div class="col-md-12">
               
              <div class="col-md-12 col-sm-12">
                <?php if(isset($_SESSION['msg'])){?> 
               	 <div class="alert alert-success alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                	<?php echo $client_lang[$_SESSION['msg']] ; ?></a> </div>
                <?php unset($_SESSION['msg']);}?>	
              </div>
            </div>
          </div>
          <div class="card-body mrg_bottom"> 
            <form action="" name="addeditcategory" method="post" class="form form-horizontal" enctype="multipart/form-data">
 
              <div class="section">
                <div class="section-body">
                   <div class="form-group" style="display:none;">
                    <label class="col-md-3 control-label">Category :-</label>
                    <div class="col-md-6">
                      <select name="cat_id" id="cat_id" class="select2 d-none"  required>
          						         						 
          							<option value="0"></option>	          							 
          						
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">GIF Image :-
                    <p class="control-label-help">(Recommended resolution: 800x600 or 900x600 or 900x700 or 600x900 or 640x960 or 680x1024)</p>
                    </label>
                    <div class="col-md-6">
                      <div class="fileupload_block">
                        <input type="file" name="gifs[]" value="" id="fileupload" multiple required>
                       <div class="fileupload_img"><img type="image" src="assets/images/add-image.png" alt="category image" /></div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Tags :-</label>
                    <div class="col-md-6">
                      <input type="text" name="tags" id="tags" value="" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">User Name :-</label>
                    <div class="col-md-6">
                      <input type="text" name="user_name" id="user_name" value="" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
        
<?php include("includes/footer.php");?>       
