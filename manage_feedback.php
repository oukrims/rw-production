<?php include("includes/header.php");

  require("includes/function.php");
  require("language/language.php");
  
    
  // Get page data
  $tableName="tbl_feedback";    
  $targetpage = "manage_feedback.php";  
  $limit = 15; 
  
  $query = "SELECT COUNT(*) as num FROM $tableName";
  $total_pages = mysqli_fetch_array(mysqli_query($mysqli,$query));
  $total_pages = $total_pages['num'];
  
  $stages = 3;
  $page=0;
  if(isset($_GET['page'])){
  $page = mysqli_real_escape_string($mysqli,$_GET['page']);
  }
  if($page){
    $start = ($page - 1) * $limit; 
  }else{
    $start = 0; 
    }


   $qry="SELECT * FROM tbl_feedback ORDER BY tbl_feedback.id DESC LIMIT $start, $limit";   
  $result=mysqli_query($mysqli,$qry);
 
  
  if(isset($_GET['feedback_id']))
  {
    Delete('tbl_feedback','id='.$_GET['feedback_id'].'');

    $_SESSION['msg']="12";
    header( "Location:manage_feedback.php");
    exit;
     
  } 
   
?>
    <div class="row">
      <div class="col-xs-12">
        <div class="card mrg_bottom">
          <div class="page_title_block">
            <div class="col-md-5 col-xs-12">
              <div class="page_title">Manage Feedback</div>
            </div>
             
          </div>
          <div class="clearfix"></div>
          <div class="row mrg-top">
            <div class="col-md-12">
               
              <div class="col-md-12 col-sm-12">
                <?php if(isset($_SESSION['msg'])){?> 
               	 <div class="alert alert-success alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                	<?php echo $client_lang[$_SESSION['msg']] ; ?></a> </div>
                <?php unset($_SESSION['msg']);}?>	
              </div>
            </div>
          </div>
          <div class="col-md-12 mrg-top">
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                   <th>Message</th> 
                  <th class="cat_action_list">Action</th>
                </tr>
              </thead>
              <tbody>
              	<?php
						$i=0;
						while($row=mysqli_fetch_array($result))
						{
						 
				?>
                <tr>
                  <td><?php echo $row['name'];?></td>
                  <td><?php echo $row['email'];?></td>
                   <td><?php echo $row['message'];?></td>                  
                  <td>
                    <a href="?feedback_id=<?php echo $row['id'];?>" onclick="return confirm('Are you sure you want to delete this feedback?');" class="btn btn-default">Delete</a></td>
                </tr>
               <?php
						
						$i++;
						}
			   ?>
              </tbody>
            </table>
          </div>
          <div class="col-md-12 col-xs-12">
            <div class="pagination_item_block">
              <nav>
              	<?php include("pagination.php");?>                 
              </nav>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>            
               
        
<?php include("includes/footer.php");?>       
