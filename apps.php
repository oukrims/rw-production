<!DOCTYPE html>
<?php
error_reporting(0);
ob_start();
session_start();

header("Content-Type: text/html;charset=UTF-8");


include "./includes/connection.php";

mysqli_query($mysqli,"SET NAMES 'utf8'");	 



//Settings
$setting_qry="SELECT * FROM tbl_settings where id='1'";
$setting_result=mysqli_query($mysqli,$setting_qry);
$settings_details=mysqli_fetch_assoc($setting_result);

define("APP_NAME",$settings_details['app_name']);
define("APP_LOGO",$settings_details['app_logo']);

define("API_LATEST_LIMIT",$settings_details['api_latest_limit']);
define("API_CAT_ORDER_BY",$settings_details['api_cat_order_by']);
define("API_CAT_POST_ORDER_BY",$settings_details['api_cat_post_order_by']);


//Profile
if(isset($_SESSION['id']))
{
$profile_qry="SELECT * FROM tbl_admin where id='".$_SESSION['id']."'";
$profile_result=mysqli_query($mysqli,$profile_qry);
$profile_details=mysqli_fetch_assoc($profile_result);

define("PROFILE_IMG",$profile_details['image']);
}

define("APP_FCM_KEY",$settings_details['app_fcm_key']);

	require("includes/function.php");
	require("language/language.php");

	require_once("thumbnail_images.class.php");

  

  $qry="SELECT * FROM apps ";
  $result= mysqli_query($mysqli,$qry);
  ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">
    <style>
    #loader {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: #ecf0f1;
}

.dot {
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  background: #2ecc71;
  border-radius: 100%;
  animation-duration: 1s;
  animation-name: loader_dot;
  animation-iteration-count: infinite;
  animation-direction: alternate;
}

@keyframes loader_dot {
  0% {
    width: 0px;
    height: 0px;
  }

  to {
    width: 50px;
    height: 50px;
  }
}
    </style>
   
</head>
<body>
        <div class="container">
       

        <?php
        
        
        if (mysqli_num_rows($result) > 0){
            // output data of each row
            while($row = mysqli_fetch_assoc($result)){    
                  echo  '<div class="card" style="margin-top:9px;">';
                echo '  <img class="card-img-top" src="'.$row['app_icon'].'" alt="Card image cap">';
                echo '  <div class="card-body">';
                echo '    <h5 class="card-title">'.$row['app_name'].'</h5> ';
                echo '    <p class="card-text">'.$row['app_desc'].'</p>';
                echo '    <a href="market://details?id='.$row['app_url'].'"  target="_top" class="btn btn-primary" style="float:right; background-color:#7A9E43; color:#fff;">Install</a>  ';
                echo '</div></div>';
            }
        }else {
            echo "0 results";
        }
        mysqli_close($conn);


        ?>     
<div id="loader">
  <div class="dot"></div>
</div>
        
</div>
        </div>
        <script
  src="http://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
    <script>
jQuery(window).load(function() {
  $('#loader').hide();
});
</script>
</body>
</html>
