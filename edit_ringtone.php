<?php include("includes/header.php");

	require("includes/function.php");
	require("language/language.php");

	require_once("thumbnail_images.class.php");

  //Get Category
	$cat_qry="SELECT * FROM tbl_ringtone_category ORDER BY category_name";
	$cat_result=mysqli_query($mysqli,$cat_qry); 

  $qry="SELECT * FROM tbl_ringtone where id='".$_GET['ringtone_id']."'";
  $result=mysqli_query($mysqli,$qry);
  $row=mysqli_fetch_assoc($result);
	
	if(isset($_POST['submit']))
	{
        
               
        
        if ($_POST['ringtone_type']=='server_url')
        {
              $ringtone_url=$_POST['ringtone_url'];

            if($_FILES['ringtone_thumbnail']['name']!="")
            { 
              $file_name= str_replace(" ","-",$_FILES['ringtone_thumbnail']['name']);

              $ringtone_thumbnail=rand(0,99999)."_".$file_name;
       
              //Main Image
              $tpath1='images/'.$ringtone_thumbnail;        
              $pic1=compress_image($_FILES["ringtone_thumbnail"]["tmp_name"], $tpath1, 80);
         
              //Thumb Image 
              $thumbpath='images/thumbs/'.$ringtone_thumbnail;   
              $thumb_pic1=create_thumb_image($tpath1,$thumbpath,'200','200');   
            }
            else
            {
              $ringtone_thumbnail=$_POST['ringtone_thumbnail_name'];
            }
 
  
        } 

        if ($_POST['ringtone_type']=='local')
        {
            if($_POST['mp3_file_name']!="")
            {
                $file_path = 'http://'.$_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']).'/uploads/';
              
                $ringtone_url=$file_path.$_POST['mp3_file_name'];

            }
            else
            {
                $ringtone_url=$_POST['ringtone_url'];
            }
            
            if($_FILES['ringtone_thumbnail']['name']!="")
            { 
              $file_name= str_replace(" ","-",$_FILES['ringtone_thumbnail']['name']);
               
              $ringtone_thumbnail=rand(0,99999)."_".$file_name;
       
              //Main Image
              $tpath1='images/'.$ringtone_thumbnail;        
              $pic1=compress_image($_FILES["ringtone_thumbnail"]["tmp_name"], $tpath1, 80);
         
              //Thumb Image 
              $thumbpath='images/thumbs/'.$ringtone_thumbnail;   
              $thumb_pic1=create_thumb_image($tpath1,$thumbpath,'200','200');   
            }
            else
            {
              $ringtone_thumbnail=$_POST['ringtone_thumbnail_name'];
            }

              $video_id='';
        } 


          
       $data = array( 
          'user_id'  =>  $_SESSION['id'],
          'ringtone_type'  =>  $_POST['ringtone_type'],
          'ringtone_title'  =>  addslashes($_POST['ringtone_title']),
          'ringtone_url'  =>  $ringtone_url,
          'ringtone_thumbnail'  =>  $ringtone_thumbnail,
          'ringtone_duration'  =>  $_POST['ringtone_duration']
          );
  		 		 
    $qry=Update('tbl_ringtone', $data, "WHERE id = '".$_POST['ringtone_id']."'");

         
		$_SESSION['msg']="11"; 
		header( "Location:edit_ringtone.php?ringtone_id=".$_POST['ringtone_id']);
		exit;	

		 
	}
	
	  
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>
            $(function () {
                $('#btn').click(function () {
                    $('.myprogress').css('width', '0');
                    $('.msg').text('');
                    var mp3_local = $('#mp3_local').val();
                    if (mp3_local == '') {
                        alert('Please enter file name and select file');
                        return;
                    }
                    var formData = new FormData();
                    formData.append('mp3_local', $('#mp3_local')[0].files[0]);
                    $('#btn').attr('disabled', 'disabled');
                     $('.msg').text('Uploading in progress...');
                    $.ajax({
                        url: 'uploadscript_mp3.php',
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        // this part is progress bar
                        xhr: function () {
                            var xhr = new window.XMLHttpRequest();
                            xhr.upload.addEventListener("progress", function (evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);
                                    $('.myprogress').text(percentComplete + '%');
                                    $('.myprogress').css('width', percentComplete + '%');
                                }
                            }, false);
                            return xhr;
                        },
                        success: function (data) {
                         
                            $('#mp3_file_name').val(data);
                            $('.msg').text("File uploaded successfully!!");
                            $('#btn').removeAttr('disabled');
                        }
                    });
                });
            });
        </script>
<script type="text/javascript">
$(document).ready(function(e) {
           $("#ringtone_type").change(function(){
          
           var type=$("#ringtone_type").val();
              
              if(type=="server_url")
              { 
                //alert(type);
                $("#video_url_display").show();
                $("#video_local_display").hide();
                $("#thumbnail").hide();
              } 
              else
              {                 
                $("#video_url_display").hide();               
                $("#video_local_display").show();
                $("#thumbnail").show();

              }    
              
         });
        });
</script>
<div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="page_title_block">
            <div class="col-md-5 col-xs-12">
              <div class="page_title">Edit Ringtone</div>
            </div>
          </div>
          <div class="clearfix"></div>
          <div class="row mrg-top">
            <div class="col-md-12">
               
              <div class="col-md-12 col-sm-12">
                <?php if(isset($_SESSION['msg'])){?> 
               	 <div class="alert alert-success alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                	<?php echo $client_lang[$_SESSION['msg']] ; ?></a> </div>
                <?php unset($_SESSION['msg']);}?>	
              </div>
            </div>
          </div>
          <div class="card-body mrg_bottom"> 
            <form action="" name="edit_form" method="post" class="form form-horizontal" enctype="multipart/form-data">
              <input  type="hidden" name="ringtone_id" value="<?php echo $_GET['ringtone_id'];?>" />

              <div class="section">
                <div class="section-body">
                   <div class="form-group">
                    <label class="col-md-3 control-label">Category :-</label>
                    
                  </div>                   
                   <div class="form-group">
                    <label class="col-md-3 control-label">Ringtone Title :-</label>
                    <div class="col-md-6">
                      <input type="text" name="ringtone_title" id="ringtone_title" value="<?php echo stripslashes($row['ringtone_title']);?>" class="form-control" required>
                    </div>
                  </div>
                   <div class="form-group">
                    <label class="col-md-3 control-label">Ringtone duration :-</label>
                    <div class="col-md-6">
                      <input type="text" name="ringtone_duration" id="ringtone_duration" value="<?php echo $row['ringtone_duration']?>" class="form-control" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Ringtone Type :-</label>
                    <div class="col-md-6">                       
                      <select name="ringtone_type" id="ringtone_type" style="width:280px; height:25px;" class="select2" required>
                            <option value="">--Select Type--</option>
                            <option value="server_url" <?php if($row['ringtone_type']=='server_url'){?>selected<?php }?>>From Server</option>
                            <option value="local" <?php if($row['ringtone_type']=='local'){?>selected<?php }?>>From Local</option>
                      </select>
                    </div>
                  </div>
                  <div id="video_url_display" class="form-group" <?php if($row['ringtone_type']=='local'){?>style="display:none;"<?php }else{?>style="display:block;"<?php }?>>
                    <label class="col-md-3 control-label">Ringtone URL :-</label>
                    <div class="col-md-6">
                      <input type="text" name="ringtone_url" id="ringtone_url" value="<?php echo $row['ringtone_url']?>" class="form-control">
                    </div>
                  </div>
                  <div id="video_local_display" class="form-group" <?php if($row['ringtone_type']=='local'){?>style="display:block;"<?php }else{?>style="display:none;"<?php }?>>
                    <label class="col-md-3 control-label">Ringtone Upload :-</label>
                    <div class="col-md-6">
                    
                    <input type="hidden" name="mp3_file_name" id="mp3_file_name" value="" class="form-control">
                      <input type="file" name="mp3_local" id="mp3_local" value="" class="form-control">
                      <div><label class="control-label">Current URL :-</label><?php echo $row['ringtone_url']?></div><br>
                      <div class="progress">
                            <div class="progress-bar progress-bar-success myprogress" role="progressbar" style="width:0%">0%</div>
                        </div>

                        <div class="msg"></div>
                        <input type="button" id="btn" class="btn-success" value="Upload" />
                    </div>
                  </div><br>
                  <div id="thumbnail" class="form-group" <?php if($row['ringtone_type']=='server_url' or $row['ringtone_type']=='local'){?>style="display:block;"<?php }else{?>style="display:none;"<?php }?>>
                    <label class="col-md-3 control-label">Thumbnail Image:-</label>
                    <div class="col-md-6">
                      <div class="fileupload_block">
                        <input type="file" name="ringtone_thumbnail" value="" id="fileupload">
                       <?php if(isset($_GET['ringtone_id']) and $row['ringtone_thumbnail']!="") {?>
                       <input type="hidden" name="ringtone_thumbnail_name" id="ringtone_thumbnail_name" value="<?php echo $row['ringtone_thumbnail'];?>" class="form-control">
                            <div class="fileupload_img"><img type="image" src="images/<?php echo $row['ringtone_thumbnail'];?>" alt="video thumbnail" /></div>
                          <?php } else {?>
                            <div class="fileupload_img"><img type="image" src="assets/images/add-image.png" alt="category image" /></div>
                          <?php }?>
                      </div>
                    </div>
                  </div>                   
                 
                  
                  <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                      <button type="submit" name="submit" class="btn btn-primary">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
        
<?php include("includes/footer.php");?>       
