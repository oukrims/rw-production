<?php   include("includes/connection.php");
        include("includes/function.php");   

        if(isset($_GET['user_id'])!="")
        { 

           $wallpaper_image= $_GET['wallpaper_image_name'];   
                 
            
              $date=date('Y-m-j');

                $data = array( 
                    'cat_id'  =>  $_GET['cat_id'],
                    'user_id'  =>  $_GET['user_id'],
                    'user_name'  =>  $_GET['user_name'],
                    'image_date'  =>  $date,
                    'image'  =>  $wallpaper_image,
                    'tags'  =>  $_GET['tags'],
                    'status'  => 0
                  );    

             
              $qry = Insert('tbl_wallpaper',$data);  

             $set['FUNDRIVE_APP'][] = array('msg'=>'Wallpaper image has been uploaded!','success'=>1);

             header( 'Content-Type: application/json; charset=utf-8' );
              echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
            die();
         }
         else
         {
          
           $file_name= str_replace(" ","-",$_FILES['wallpaper_image']['name']);
           
           $albumimgnm=$file_name;
       
           //Main Image
           $tpath1='images/'.$albumimgnm;       
           $pic1=compress_image($_FILES["wallpaper_image"]["tmp_name"], $tpath1, 80);
       
           //Thumb Image 
           $thumbpath='images/thumbs/'.$albumimgnm;        
           $thumb_pic1=create_thumb_image($tpath1,$thumbpath,'400','400');   
                  
 
            $set['FUNDRIVE_APP'][] = array('wallpaper_image_name'=>$albumimgnm,'msg'=>'Wallpaper image has been uploaded!','success'=>1);
    
            header( 'Content-Type: application/json; charset=utf-8' );
              echo $val= str_replace('\\/', '/', json_encode($set,JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT));
            die();
         }

?>